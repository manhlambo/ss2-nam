const express = require("express");
const bodyParser = require("body-parser");
const path = require("path");
const utils = require("./utils.js");

const app = express();

//define path for express config
const publicDirPath = path.join(__dirname, "../public");

//set handlebars engine
app.set("view engine", "hbs");

//setup static directory to serve
app.use(express.static(publicDirPath));

//create application/x-www-form-urlencoded parser
app.use(bodyParser.urlencoded({ extended: true }));

//Route
app.get("", (req, res) => {
  res.render("index");
});

app.get("/about", (req, res) => {
  res.render("about");
});

app.get("/contact", (req, res) => {
  res.render("contact");
});

app.post("/contact", (req, res) => {
  utils.addContactData(req.body);
  res.redirect("/contact");
});

//Error route
app.get("*", (req, res) => {
  res.render("404");
});

//start the server
const port = 3000;

app.listen(port, () => {
  console.log(`Server is up on port ${port}`);
});
