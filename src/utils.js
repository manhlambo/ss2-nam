const fs = require("fs");

const loadContactData = function () {
  try {
    const dataBuffer = fs.readFileSync("contact_data.json");
    const dataJSON = dataBuffer.toString();
    return JSON.parse(dataJSON);
  } catch (err) {
    return [];
  }
};

const addContactData = function (data) {
  const contact = loadContactData();
  contact.push(data);
  console.log(contact);
  saveData(contact);
};

const saveData = function (data) {
  const dataJSON = JSON.stringify(data);
  fs.writeFileSync("contact_data.json", dataJSON);
};

//export function
module.exports = {
  addContactData: addContactData,
};
